package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void turnNorth() {
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void turnEast(){
        myRobot.turn();
        assertEquals(Robot.Direction.East, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
     }

    @Test
    public void turnSouth() {
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void turnWest() {
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void moveNorth() throws IllegalMoveException{
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        myRobot.move();
        assertEquals(9, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void moveEast() throws IllegalMoveException{
        myRobot.turn();
        myRobot.move();
        assertEquals(10, myRobot.row());
        assertEquals(2, myRobot.column());
    }

    @Test
    public void moveSouth() throws IllegalMoveException{
        try {
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.row());
            assertEquals(1, myRobot.column());
        }catch (IllegalMoveException e){
            e.getMessage();
        }
    }

    @Test
    public void moveWest() throws IllegalMoveException{
        try {
            myRobot.turn();
            myRobot.move();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.row());
            assertEquals(1, myRobot.column());
        }catch (IllegalMoveException e){
            e.getMessage();
        }
    }

    @Test
    public void moveInvalidNorth() throws IllegalMoveException{
        try {
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            assertEquals(0, myRobot.row());
            assertEquals(1, myRobot.column());
        }catch (IllegalMoveException e){
            System.out.println("Invalid Move North");
        }
    }

    @Test
    public void moveInvalidEast() throws IllegalMoveException{
        try {
            myRobot.turn();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            myRobot.move();
            assertEquals(11, myRobot.row());
            assertEquals(10, myRobot.column());
        }catch (IllegalMoveException e){
            System.out.println("Invalid Move East");
        }
    }

    @Test
    public void moveInvalidSouth() throws IllegalMoveException{
        try {
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(11, myRobot.row());
            assertEquals(1, myRobot.column());
        }catch (IllegalMoveException e){
            System.out.println("Invalid Move South");
        }
    }

    @Test
    public void moveInvalidWest() throws IllegalMoveException{
        try {
            myRobot.turn();
            myRobot.turn();
            myRobot.turn();
            myRobot.move();
            assertEquals(10, myRobot.row());
            assertEquals(0, myRobot.column());
        }catch (IllegalMoveException e){
            System.out.println("Invalid Move West");
        }
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }
}
